System to create, manage and apply python overrides in a flexible and reliable way as they are used in the lighting process of the Blender Studio pipeline.
